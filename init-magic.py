from search import executeSearch
from emailSender import sendEmail

results = executeSearch()
print("I found " + str(len(results)) + " results")

sendEmail(results, "results.txt")
