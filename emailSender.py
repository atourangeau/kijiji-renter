import smtplib
from email.message import EmailMessage

def sendEmail(results, filename):

    if len(results) == 0:
        return

    msg = EmailMessage()
    msg['Subject'] = "Nouveaux apparts"
    msg['From'] = "email"
    msg['To'] = "email"

    msgContent = ""
    lastStation = ""

    for result in results:
        if result.metroStation != lastStation:
            msgContent = msgContent + result.metroStation + " \n"
            lastStation = result.metroStation

        msgContent = msgContent + result.price + " Électros: " + str(result.appliances) + " Laveuse/secheuse: " + str(result.washingMachine) + " Link: " + result.url + "\n"

    msg.set_content(msgContent)

    # Send the message via gmail server.
    # DO NOT FORGET TO TURN ON LESS SECURED APP ON GMAIL ACCOUNT
    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login("gmail address", "psw")

        server.send_message(msg)
        server.quit()
    except Exception:
        print("Couldn't send the email, results are in the results.txt file")
        file = open(filename, "w+")
        file.write(msgContent)
        file.close()