import requests
import smtplib
from helper import getBaseInfos
from helper import getSpecificInfo
from bs4 import BeautifulSoup
from ad import Ad
from metroStation import metroStation

def executeSearch() :
    minPrice = '600'  # if none let empty ('')
    maxPrice = '1000' # if none let empty ('')
    prefix = 'https://www.kijiji.ca'
    rayon = '4.0' # keep the '.0' synthax

    metroStationLocation = [
        metroStation('H2S2N7', '45.538051', '-73.605391', 'Jean-talon'),
        metroStation('H2R2H2', '45.543370', '-73.628407', 'Jarry'),
        metroStation('H2R2X9', '45.535655', '-73.619862', 'Castelneau'),
        metroStation('H2S2R4', '45.531375', '-73.598476', 'Rosemont')
    ]

    results = []

    for station in metroStationLocation:

        # search url
        url = prefix + '/b-appartement-condo/ville-de-montreal/c37l1700281r' + rayon + '?ll=' + station.long + ',' + station.lat + '&address=' + station.postalCode + '&price=' + minPrice + '__' + maxPrice
        print("SEARCH AROUND " + str(station.name)+ " : " + url)
        result = None
        try:
            result = requests.get(url)
        except Exception:
            print('Error with station ' + str(station.name))

        if result.status_code == 200:
            soup = BeautifulSoup(result.text, 'html5lib')

            # get all ads
            ads = soup.findAll('div', {'class':'regular-ad'})

            adsList = []

            # gather infos
            for baseAd in ads: 
                ad = getBaseInfos(baseAd)
                ad.metroStation = station.name

                # if the ad was posted less than 60 min ago
                # execute a second query to get more info
                if(ad.datePosted < 60):
                    ad.url = prefix + baseAd.attrs.get('data-vip-url')
                    ad.id = baseAd.attrs.get('data-ad-id')

                    ad = getSpecificInfo(ad)

                    adsList.append(ad)
            
        results = results + adsList

    return results
