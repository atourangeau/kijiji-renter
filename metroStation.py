class metroStation:
    postalCode: ''
    long: ''
    lat: ''
    name: ''

    def __init__(self, postalCode, long, lat, name):
        self.postalCode = postalCode
        self.long = long
        self.lat = lat
        self.name = name