
from search import executeSearch
from emailSender import sendEmail
import time

iteration = 0
while(True):

    print("\n********  Iteration #" + str(iteration) + "   ********")

    results = executeSearch()
    print("I found " + str(len(results)) + " results")

    filename = "results_" + str(iteration) + ".txt"
    sendEmail(results, filename)
    iteration = iteration + 1

    #time.sleep(3600)
    time.sleep(10)
