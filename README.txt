************* WORKS ONLY FOR MONTREAL ***************

1) 2 scripts
init-magic.py : start the search once
init-magic-forever.py : start the search every hour (you can change it for 30 min or the time you want)

2) How to add a metro station
In the search.py file, add a metroStation in the metroStationLocation array.
metroStation(postalCode, longitude, latitude, name)

3) How to change the price
In the search.py file, change minPrice and maxPrice value

4) How to change the radius
In the search.py file, change the rayon (oops) value

5) How to change the time filter
In the search.py file, change the value at line 46. *** It uses minutes only ***

6) How to change description analyzer
Right now, it sucks. But you can add key words in the analyseDescription function (helper.py file)

7) How to enable emails
In the emailSender.py file, change 'To' and 'From' emails (line 6-7)
In the same file, change the server.login("gmail address", "psw") line for YOUR email and password (You have to authenticate yourself to Gmail to send emails)
IMPORTANT -> you have to change in your gmail account -> Security -> Autorize less secured apps (activate it)
