from bs4 import BeautifulSoup
from ad import Ad
import requests

def getBaseInfos(ad):
    infos = ad.find('div', {'class':'clearfix'}).find('div', {'class':'info'}).find('div', {'class':'info-container'})
    
    adObject = Ad()
    
    adObject.price  = infos.find('div', {'class':'price'}).text.strip()
    
    dateString = infos.find('div', {'class':'location'}).find('span', {'class':'date-posted'}).text.strip()
    adObject.datePosted = parseDateFromString(dateString)

    return adObject

def getSpecificInfo(ad):
    result = requests.get(ad.url)
    soup = BeautifulSoup(result.text, 'html5lib')

    ad.location = address = soup.find('span', {'itemprop': 'address'})
    description = soup.select('div[class*="descriptionContainer-"]')[0].find('div').text
    
    ad = analyseDescription(description, ad) # check if appliances are included
    
    return ad

# Still need a LOT of improvements...
def analyseDescription(description, ad):
    if (not description.find("buanderie")):
        washingMachinKeyWords = ['laveuse', 'laveuse-sécheuse', 'laveuse-secheuse', 'laveuse/secheuse', 'laveuse/sécheuse']
        for keyWord in washingMachinKeyWords:
            if description.find(keyWord):
                ad.washingMachine = True
                break

    appliancesKeyWord = ['réfrigérateur', 'refrigerateur', 'four', 'électro', 'electro']
    for keyWord in appliancesKeyWord:
        if description.find(keyWord):
            ad.appliances = True
            break

    return ad

def parseDateFromString(dateString):

    prefix = "Il y a moins de"
    if dateString.find(prefix) == -1 :
        return -1
    
    datestring = dateString.replace(prefix, "").split()

    if datestring[1] == 'minutes':
        return int(datestring[0])
    else:
        return int(datestring[0]) * 60
    


